# Annex 4: Data formats

## 3.1.3.1: Data formats

### Requirements

* Disclosure of data formats
  * Submitting the manuals or technical data sheets in which the data formats are documented **_or_**
  * Providing examples of other software products (from other suppliers) that can process these data formats **_or_**
  * Stating the data formats and assigning them to an open standard.

### Documentation of data formats

KMail supports a wide range of email related standards which are covering most of the use cases in sending, receiving, displaying, signing, encrypting, and storing email.

Central specific open standards supported for interconnecting with other email servers and other email clients:

* IMAP ([RFC 3501](https://datatracker.ietf.org/doc/html/rfc3501))
* POP3 ([RFC 1939](https://datatracker.ietf.org/doc/html/rfc1939))
* mbox ([RFC 4155](https://datatracker.ietf.org/doc/html/rfc4155))
* maildir ([maildir format](https://cr.yp.to/proto/maildir.html))

KMail supports importing and exporting emails between different email clients, so that users can choose and migrate as they prefer.

Exact details of each format are fully available for inspection because the source code of its implementation is available as open source.
