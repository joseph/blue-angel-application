This is the working directory for creating the application for the Blue Angel label for KMail.

## Resources

* [KMail Website](https://kontact.kde.org/components/kmail.html) ([Sources](https://invent.kde.org/websites/kontact-kde-org))
* [KMail Handbook](https://docs.kde.org/stable5/en/kmail/kmail2/) ([PDF](https://docs.kde.org/stable5/en/kmail/kmail2/kmail2.pdf)) ([Sources](https://invent.kde.org/pim/kmail/-/tree/master/doc/kmail2))
* [KMail Wiki page on UserBase](https://userbase.kde.org/KMail)
* [KMail on apps.kde.org](https://apps.kde.org/kmail2)
* [KMail source code](https://invent.kde.org/pim/kmail)
