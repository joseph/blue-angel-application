# Annex 6: Product information

## 3.1.3.3: Continuity of the software product

### Requirements

* Security updates must be provided free of charge
* Security updates for the labelled product for at least 5 years after the end of sale
* The user must be given the option of whether to install only security updates or also other (e.g. functional) updates.

### Product information in which fullfilement of requirements is documented

KMail is released as a part of Kontact, KDE's Personal Information Management suite. Kontact has a history of continuous incremental updates as open source product for 17 years starting in 2004. All updates always have been and always will be released free of charge.

KMail is released as part of the collection of KDE application the KDE project releases on a [4 month release schedule](https://community.kde.org/Schedules). From April 2021 on this set is called "KDE Gear". In the years before it was mainly known as "KDE Applications". This release schedule is maintained by the KDE community and supported by KDE e.V. The community is committed to doing this indefinitely.

As the source code is released as open source, it always will be possible for others to pick up KMail releases and continue them. KMail is shipped by many Linux distributions which give a variety of different support levels. Some distributions offer multi-year plans. See also the [packaging status](https://repology.org/project/kmail/versions).

The openness of the code, the proven sustainable community and organization behind it, which by its nature is more independent of economic challenges than commercial companies, and the diversity of third parties vendors which distribute KMail in various forms guarantee KMail's continuity as a software product over a long period of time, going way beyond a 5 year perspective.

The continuous incremental releases make sure users reliably get a working product with latest security updates. This includes the changes necessary to adapt to releases of dependencies which fix security related problems. Functional changes are incremental and it wouldn't serve users to separate them from pure security updates becuase users would be blocked from critical fixes and security updates of dependencies.

Because of the code being released as open source code users also always have the option to maintain their own branch of KMail, backport the fixes they need or add their own ones. This could also be done by one of the many distributors who ship versions of KMail for the users who are not capable or willing to do it themselves.

In any case continuity is guaranteed in principle forever by the chosen open source license.

## 3.1.3.4: Uninstallability

### Requirement

* Residue-free uninstallability of the software

### Product information in which fullfilement of requirements is documented

KMail can be uninstalled without leaving residues on the system. The exact procedure depends on how it has been installed.

When installing from sources, KMail can be uninstalled with the command `make uninstall`. This is the standard procedure provided by the common build tool CMake.

When installing binary packages, the instructions of the package provider apply. With Linux distributions there usually is a command line tool which has some kind of uninstall command. They also usually offer graphical applications to uninstall packages.

The data KMail handles is completely separate from the program. It's under full control of the user. An uninstallation won't touch any user-generated data.

## 3.1.3.5: Offline capability

### Requirement

* The functionality and availability of the software must not be negatively influenced by external factors, such as the availability of a licence server.

### Product information in which fullfilement of requirements is documented

KMail as an email application needs network access for sending and receiving email as this is the primary purpose of the application. In addition to that no network access is needed, neither for a license server nor for sending user data to a vendor or something similar.

KMail has an offline mode where users can work with their emails even when not connected to a network. Only sending and receiving new emails requires the network as email transmission is a network operation by definition.

## 3.1.3.6: Modularity

### Requirements

* Information on how individual modules of the software product can be deactivated during the installation process.
* Information on the extent to which individual modules of the software product (especially those that do not belong to the functions of the software product such as tracking, etc.) can be deactivated during the use of the software product.

### Product information in which fullfilement of requirements is documented

KMail is a single purpose application as an email client. It comes as part of the Personal Information Management suite [Kontact](https://kontact.kde.org/, which also includes modules for handling calendars, addressbooks, notes and other data. The user has full control, which of these modules are installed and enabled. Interaction between modules is automatically limited to the ones the user has chosen. Configuration of modules (including KMail) in Kontact is described in the [Kontact manual](https://docs.kde.org/stable5/en/kontact/kontact/configuration.html#main-config).

When KMail is installed from packages built by Linux distributions or other packagers, they usually are built in a modular way, so that for example translations of the user interface or the manuals can be installed separately, in the configuartion chosen by the user.

## 3.1.3.8: Documentation of the software product, licence conditions and terms of use

### Requirement

Information about the software product both publicly and also in combination with the product itself:

a) Description of the processes for installing and uninstalling the software
b) Description of the data import and export processes
c) Information on reducing the use of resources
d) Information on the licensing terms and terms of use to enable, where relevant, the legally compliant further development of the software product
e) Information on software support
f) Information on the handling of data, in the sense of existing data protection laws
g) Information on data security, data collection and data transmission

### Product information in which fullfilement of requirements is documented

a) Description of the processes for installing and uninstalling the software

KMail can be installed in several ways, depending on preferences of users and which platforms they use. Specific instructions for installing and uninstalling are part of the instructions and documentation of the general platforms used to handle the specific KMail packages chosen by users. KMail gives users full control here by enabling them to chose their preferred installation mechanism. Generic documentation which is applicable to KMail is available as [tutorial for installing KDE applications](https://userbase.kde.org/Tutorials/Install_KDE_software) on [KDE UserBase](https://userbase.kde.org).

b) Description of the data import and export processes

KMail supports various standard protocols for email exchange. It also supports various formats for importing and exporting email data. See Annex 4 for more details.

c) Information on reducing the use of resources

KMail is designed to make effective use of resources. It focuses on the core functionality of viewing documents and doesn't include any functionality which would consume resources going beyond what is necessary to serve this core purpose.

d) Information on the licensing terms and terms of use to enable, where relevant, the legally compliant further development of the software product

KMail is Free Software and released under the GNU Public License which allows unlimited use and modifications and distribution of changes as long as this is done under the GPL as well. This is stated in the [KMail Manual](https://docs.kde.org/stable5/en/kmail/kmail2/credits-and-licenses.html). The source code for KMail is publically available: https://invent.kde.org/pim/kmail. The full text of the detailed licenses used in KMail are part of the code and are required to be part of any distribution of KMail: https://invent.kde.org/pim/kmail/-/tree/master/LICENSES.

e) Information on software support

KMail is supported by the [KDE community](https://kde.org). Specific forums and points of contacts for KMail are available and documented as part of the [Kontact website](https://kontact.kde.org/users.html).

f) Information on the handling of data, in the sense of existing data protection laws

KMail is fully compliant with existing data protection laws. The full privacy policy can be found here: https://kde.org/privacypolicy-apps/. As an email client KMail deals with personal data, but doesn't collect any more data than covered by the email services it is connected to. The privacy policies of these services have to be reviewed by the user when creating accounts there. They are out of scope of what KMail can control and KMail doesn't add any additional services for processing data.

g) Information on data security, data collection and data transmission

Full documentation of data handling can be found in the KDE privacy policy: https://kde.org/privacypolicy-apps/. It also references the KDE Telemetry policy, which describes how telemetry data is collected in a way which fully protects user's privacy: https://community.kde.org/Policies/Telemetry_Policy.

KMail doesn't collect personal data and specifically doesn't transmit any data to other systems or parties other than what is required to fullfil its functionality as an email client. There are no ads, no user tracking. Users are always in full control of what they do with their data.

## 3.2.1: Requirements for the further development and update of the product

### Requirement

If the product is changed (e.g. through updates), it must be ensure that the software product still complies with all of the criteria.

### Product information in which fullfilement of requirements is documented

Future updates are not expected to change the product in a way which significantly affects any of the requirements.
