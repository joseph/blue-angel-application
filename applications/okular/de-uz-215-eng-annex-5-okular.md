# Annex 5: Open standards

## 3.1.3.2: Transparency of the software product

### Requirement

* The APIs should correspond to open standards.

## Documentation

*Interface documentation, permalink to the software source code on a source code management platform, software licences or similar*

Okular is released as open source code under the GPL v2 license. Its code has always been fully transparent, and development is taking place in the open. All APIs are documented. Code can be inspected and modified by any user.

* [Okular API documentation](https://api.kde.org/okular/html/index.html)
* [Okular source code](https://invent.kde.org/graphics/okular)
* [Okular license](https://invent.kde.org/graphics/okular/-/blob/master/COPYING)
