# Intro: Submitter Info

/label ~"Annex 1"

* [ ] Applicant
* [ ] Distributor (label user)
* [ ] Brand / trade name
* [ ] Product designation
* [ ] Product description

# 3.1.1.1: Minimum system requirements

/label ~"Annex 1" ~"Annex 2"

*Note: Enter data in Annex 2 and then copy it to intro in Annex 1*

* [ ] Minimum processor architecture incl. generation
* [ ] Minimum local working memory required (MByte)
* [ ] Minimum local permanent storage required (MByte)
* [ ] Requirements for other software
* [ ] The required external services that are not available on the reference system
* [ ] The required additional hardware

# 3.1.1.2: Hardware utilisation and electrical power consumption in idle mode

/label ~"Annex 1" ~"Annex 2" ~"Annex 3"

## Annex 3

* [ ] Save OSCAR report for idle scenario run (Prefix file with `annex3-`. Filenames to be filled in Annex 1.)

## Annex 2

* [ ] Fill "Calculation of the criteria" sheet
  * [ ] Full load
    * [ ] Processor: 100%
    * [ ] Memory: installed RAM (run `free` and take total Mem)
    * [ ] Storage: 6 Gbit/s or 750 MB/s for the reference systems
  * [ ] Base load (copy from OSCAR report)
    * [ ] Processor utilisation (from summary table "Gemessener Wert (Baseline)" - "Mittlere CPU-Auslastung"),
    * [ ] Memory utilisation (from summary table "Gemessener Wert (Baseline)" - "Mittlere RAM-Auslastung"
    * [ ] Storage utliisation: (performanceBaseline$HDDRead.mean + performanceBaseline$HDDWritten.mean) / 1024
    * [ ] Network utilisation: (performanceBaseline$networkSent.mean + performanceBaselineNetworkReceived.mean) / 1024
  * [ ] Idle load
    * [ ] Processor utilisation (from summary table "Gemessener Wert (Leerlauf)" - "Mittlere CPU-Auslastung")
    * [ ] Memory utilisation: performanceMeasurement$ram.mean / 1024
    * [ ] Storage utilisation: (performanceMeasurements$HDDRead.mean + performanceMeasurements$HDDWritten.mean) / 1024
    * [ ] Network utilisation: (performanceMeasurement$networkSent.mean + performanceMeasurementNetworkReceived.mean) / 1024
* [ ] Fill section 3.1.1.2 on "Requirements" sheet
  * [ ] Copy data to 3.1.1.2 section a) to d) from "Calculation of the criteria" sheet
  * [ ] Fill 3.1.1.2 e): From overview section of OSCAR report "Mittlere el. Arbeit": "Gemessener Wert (Leerlauf)" - "Gemessener Wert (Baseline)"

## Annex 1

* [ ] Copy data from Annex 2 to intro in Annex 1
  * [ ] Average processor utilisation in idle mode (%)
  * [ ] Average working memory utilization in idle mode (Mbyte)
  * [ ] Average permanent storage utilization in idle mode (MByte/s)
  * [ ] Average bandwidth utilization for network access in idle mode (Mbit/s) (only contains the additional load, not the percentage share of the base load)
  * [ ] Average electrical power consumption (net) (W)

**Requirement**

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Spreadsheet file (Annex 2)
* [ ] Measurement report (Annex 3)

# 3.1.1.3: Hardware utilisation and energy demand when running a standard usage scenario

/label ~"Annex 1" ~"Annex 2" ~"Annex 3"

## Annex 3

* [ ] Save OSCAR report for usage scenario run under filename `annex-3-<appname>-report-scenario.pdf`. Filename will be recorded in Annex 1.

## Annex 2

* [ ] Fill "Calculation of the criteria" sheet
  * [ ] Full load (copy from idle mode section)
  * [ ] Base load (copy from idle mode section)
  * [ ] Gross load
    * [ ] Processor utilisation (from summary table "Gemessener Wert (Szenario)" - "Mittlere CPU-Auslastung")
    * [ ] Memory utilisation (from summary table "Gemessener Wert (Szenario)" - "Mittlere RAM-Auslastung")
    * [ ] Storage utilisation: (performanceMeasurements$HDDRead.mean + performanceMeasurements$HDDWritten.mean) / 1024
    * [ ] Network utilisation: (performanceMeasurement$networkSent.mean + performanceMeasurementNetworkReceived.mean) / 1024
  * [ ] Measuring period
* [ ] Fill section 3.1.1.3 on "Requirements" sheet
  * [ ] Copy data to 3.1.1.3 section a) to d) from "Calculation of the criteria" sheet
  * [ ] Fill 3.1.1.3 e): From overview section of OSCAR report "Mittlere el. Arbeit": "Gemessener Wert (Szenario)" - "Gemessener Wert (Baseline)"

## Annex 1

* [ ] Copy data from Annex 2 to intro in Annex 1
  * [ ] Processor utilisation (%*s)
  * [ ] Working memory utilisation (MByte*s)
  * [ ] Permanent storage utilisation (reading and writing) (MByte/s*s)
  * [ ] Volume of data transferred for network access (Mbit/s*s)
  * [ ] Average energy demand (net) (Wh)

**Requirement**

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Spreadsheet file (Annex 2)
* [ ] Measurement report (Annex 3)

# 3.1.1.4: Support for the energy management system

/label ~"Annex 1"

**Requirement**: The software product must be capable of unrestricted functional use with activated energy management of the underlying system layers or the connected client systems.

* [ ] Compliance with the requirement is confirmed.

# 3.1.2.1: Backward compatibility

/label ~"Annex 1" ~"Annex 2"

**Requirement**: The software product must be able to run on a reference system from a calendar year that is at least five years before time of application.

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Spreadsheet file (Annex 2)
* [ ] Calendar year

# 3.1.3.1: Data formats

/label ~"Annex 1" ~"Annex 4"

**Requirement**: Disclosure of data formats

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Documentation in Annex 4
  * [ ] Submitting the manuals or technical data sheets in which the data formats are documented **_or_**
  * [ ] Providing examples of other software products (from other suppliers) that can process these data formats **_or_**
  * [ ] Stating the data formats and assigning them to an open standard.

# 3.1.3.2: Transparency of the software product

/label ~"Annex 1" ~"Annex 5"

**Requirement:** The APIs should correspond to open standards.

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Interface documentation, permalink to the software source code on a source code management platform, software licences or similar (Annex 5)

# 3.1.3.3: Continuity of the software product

/label ~"Annex 1" ~"Annex 6"

**Requirement:** Security updates must be provided free of charge

* [ ] Compliance with the requirement is confirmed.

**Requirement:** Security updates for the labelled product for at least 5 years after the end of sale

* [ ] Compliance with the requirement is confirmed.

**Requirement:** The user must be given the option of whether to install only security updates or also other (e.g. functional) updates.

* [ ] Compliance with the requirement is confirmed.

**Documentation:**

* [ ] Product information in which software updates are indicated (Annex 6)

# 3.1.3.4: Uninstallability

/label ~"Annex 1" ~"Annex 6"

**Requirement:** Residue-free uninstallability of the software

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Product information in which the process for uninstalling the software is indicated (Annex 6)

**Requirement:** Created and processed data may not be automatically deleted (exception: the user confirms the deletion)

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Product information in which the process for uninstalling the software is indicated (Annex 6)

# 3.1.3.5: Offline capability

/label ~"Annex 1" ~"Annex 6"

**Requirement:** The functionality and availability of the software must not be negatively influenced by external factors, such as the availability of a licence server.

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Product information in which the offline capability is indicated (Annex 6)

# 3.1.3.6: Modularity

/label ~"Annex 1" ~"Annex 6"

**Requirement:** Information on how individual modules of the software product can be deactivated during the installation process.

* [ ] Compliance with the requirement is confirmed.

**Requirement:** Information on the extent to which individual modules of the software product (especially those that do not belong to the functions of the software product such as tracking, etc.) can be deactivated during the use of the software product.

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Product information in which the modularity is indicated (Annex 6)

# 3.1.3.7: Freedom from advertising

/label ~"Annex 1"

**Requirement:** Free of advertising

* [ ] Compliance with the requirement is confirmed.

# 3.1.3.8: Documentation of the software product, licence conditions and terms of use

/label ~"Annex 1" ~"Annex 6" ~"Annex 7"

**Requirement:** Information about the software product both publicly and also in combination with the product itself:

a) Description of the processes for installing and uninstalling the software

b) Description of the data import and export processes

c) Information on reducing the use of resources

d) Information on the licensing terms and terms of use to enable, where relevant, the legally compliant further development of the software product

e) Information on software support

f) Information on the handling of data, in the sense of existing data protection laws

g) Information on data security, data collection and data transmission

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Relevant pages of the product information (Annex 6)

**Requirement:** Contents of the spreadsheet file for the collection of criteria (Annex 2) will be published on the website of the respective product

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] The data are attached to a defined data format (see Appendix C) (Annex 7)

# 3.1.3.8 Energy data in XML format

/label ~"Annex 7"

Provide energy measurement data from Annex 2 spreadsheet in standard XML format (defined in Appendix C of the Blue Angel specification).

**Documentation**

* [ ] The data are attached to a defined data format (see Appendix C) (Annex 7)

# 3.2.1: Requirements for the further development and update of the product

/label ~"Annex 1" ~"Annex 2" ~"Annex 6" ~"Annex 7"

**Requirement:** If the product is changed (e.g. through updates), it must be ensure that the software product still complies with all of the criteria.

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Spreadsheet file for recording the criteria (Annex 2)
* [ ] Productinformation (Annex 6)
* [ ] The data are attached to a defined data format (see Appendix C) (Annex 7)

# 3.3.1: Final evaluation

/label ~"Annex 1"

**Requirement:** A resource efficiency report is submitted at the end of the term of the contract.

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Addition to the spreadsheet file

# Details of the analysed product

/label ~"Annex 2"

* [ ] Product
* [ ] Version
* [ ] Manufacturer
* [ ] Software class (architecture)

# Measurement information

/label ~"Annex 2"

* [ ] Date of the measurement

**Details of the person who performed the measurement:**

* [ ] Name
* [ ] Mail
* [ ] Other

**Institute:**

* [ ] Designation
* [ ] Addresse
* [ ] Website
* [ ] Other

**Notes:**

* [ ] Notes on the measurement

# Technical details of the measurement

/label ~"Annex 2"

* [ ] Measuring instrument
* [ ] Sampling frequency
* [ ] Length of the scenario
* [ ] Sample size

# Information on the reference system

/label ~"Annex 2"

* [ ] Reference system year (see Award Criteria, Annex D)
* [ ] Type of hardware (see hint)
* [ ] Model
* [ ] Processor
* [ ] Cores
* [ ] Clock speed
* [ ] RAM
* [ ] Hard disk  (SSD / HDD)
* [ ] Graphics card
* [ ] Network
* [ ] Cache
* [ ] Mainboard
* [ ] Operating system

# Information on the software stack (measuring system)

/label ~"Annex 2"

* [ ] Information

# Standard use case scenario

/label ~"Annex 2"

* Info
  * [ ] Software (Name)
  * [ ] Produktgruppe (z.B. Textverarbeitung)
  * [ ] Stand (Datum)
  * [ ] Bezeichnung
  * [ ] Autoren 
  * [ ] Softwareklasse
  * [ ] SUT
  * [ ] Lastgenerator
* [ ] Prerequisites
* [ ] Procedure
* [ ] At the end of the measurement
