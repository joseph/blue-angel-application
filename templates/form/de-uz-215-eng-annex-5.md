# Annex 5: Open standards

## 3.1.3.2: Transparency of the software product

### Requirement

* The APIs should correspond to open standards.

## Documentation

*Interface documentation, permalink to the software source code on a source code management platform, software licences or similar*
